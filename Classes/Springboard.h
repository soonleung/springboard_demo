//
//  SpringboardViewController.h
//  demo
//
//  Created by Soon Leung on 01/02/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SpringboardDelegate;

@class SpringboardItem;

@interface Springboard : UIView	{
	UIScrollView			*_customizeView;
	
    id<SpringboardDelegate> _delegate;
    NSArray					*_items;
	BOOL					_isCustomizing;

	int						heldFrameIndex;
	SpringboardItem			*_heldItem;
	NSMutableArray			*_itemFrameContainer;
	NSMutableArray			*_itemForFrameRearrangeContainer;
	
	BOOL					adjustingPage;
}

@property(nonatomic,assign) id<SpringboardDelegate> delegate;
@property(nonatomic,copy)   NSArray					*items; 

@property BOOL isCustomizing;

- (void)beginCustomizing:(UITouch *)touch;
- (void)endCustomizing;

-(void)buttonAction:(id)sender;
-(void)adjustToCurrentPage;

@end


//___________________________________________________________________________________________________

@protocol SpringboardDelegate<NSObject>
@optional

- (void)springboard:(Springboard *)sp didSelectItem:(SpringboardItem *)item;

/* called when user begins or ends customization. you can use the 'willEnd' to set up what appears underneath. 
 changed is YES if there was some change to which buttons are visible or which order they appear. If selectedButton is no longer visible, 
 it will be set to nil.
 */

- (void)springboard:(Springboard *)sp willBeginCustomizingItems:(NSArray *)items;                     // called before customization begin. buttons is current button list
- (void)springboard:(Springboard *)sp didEndCustomizingItems:(NSArray *)items;  // called after customization end. buttons is new button list


@end
