//
//  SpringboardItem.m
//  demo
//
//  Created by Soon Leung on 01/02/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SpringboardItem.h"

#import "Springboard.h"

#import <QuartzCore/QuartzCore.h>


@implementation SpringboardItem

@synthesize delegate = _delegate;
@synthesize title = _title;

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
		[self setMultipleTouchEnabled:YES];
    }
    return self;
}

- (id)initWithTitle:(NSString *)title image:(UIImage *)image {
	CGRect aFrame = CGRectZero;
	aFrame.size = ITEM_FRAME_SIZE;	
	id object = [self initWithFrame:aFrame];
	
	// Initialization code
	_icon = [UIButton buttonWithType:UIButtonTypeCustom];
	_icon.frame = CGRectMake(0, 0, image.size.width, image.size.height);
	[_icon setImage:image forState:UIControlStateNormal];
	_icon.userInteractionEnabled = NO;
	[self addSubview:_icon];
	
	self.title = title;
	
	UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(_icon.frame), ITEM_FRAME_SIZE.width, 25)];
	label.backgroundColor = [UIColor clearColor];
	label.font = [UIFont boldSystemFontOfSize:12];
	label.numberOfLines = 2;
	label.textAlignment = NSTextAlignmentCenter;
	[self addSubview:label];
	[label release];
	label.text = title;
	
	return object;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)dealloc {
	[_icon release];
	[_title release];
	
    [super dealloc];
}

#pragma mark -
#pragma mark UIResponder methods overide
-(void)checkLongPress:(UITouch *)touch {
	if (touch.phase == UITouchPhaseStationary) {
		if ([_delegate respondsToSelector:@selector(beginCustomizing:)]) {
			[_delegate performSelector:@selector(beginCustomizing:) withObject:touch];
		}
	}
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event { 
	[_icon setHighlighted:YES];
	
	UITouch *touch = [touches anyObject]; 
	
	if (!((Springboard *)_delegate).isCustomizing) {
		[self performSelector:@selector(checkLongPress:) withObject:touch afterDelay:1.0];
	}
	else {
		[_delegate touchesBegan:touches withEvent:event];
	}
} 
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event { 
	UITouch *touch = [touches anyObject]; 

	[NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(checkLongPress:) object:touch];
	[_delegate touchesMoved:touches withEvent:event];
} 
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	[_icon setHighlighted:NO];
	
	if (((Springboard *)_delegate).isCustomizing) {
		[_delegate touchesEnded:touches withEvent:event];
	}
	else {
		UITouch *touch = [touches anyObject]; 
		if ([touch locationInView:_delegate].x == [touch previousLocationInView:_delegate].x &&
			[touch locationInView:_delegate].y == [touch previousLocationInView:_delegate].y) {
			[_delegate buttonAction:self];
		}
		else {
			[_delegate adjustToCurrentPage];
		}
	}
}

#pragma mark -
#pragma mark Animation during customization
- (void)appearDraggable {
    self.layer.opacity = 0.6;
	
	[UIView beginAnimations:nil context:nil];
    [self.layer setValue:[NSNumber numberWithFloat:1.25] forKeyPath:@"transform.scale"];
	[UIView commitAnimations];
}
- (void)appearNormal {
    self.layer.opacity = 1.0;
	
	[UIView beginAnimations:nil context:nil];
    [self.layer setValue:[NSNumber numberWithFloat:1.0] forKeyPath:@"transform.scale"];
	[UIView commitAnimations];
}
- (void)startWiggling {
    CAKeyframeAnimation *anim = [CAKeyframeAnimation animationWithKeyPath:@"transform.rotation"];
    anim.values = [NSArray arrayWithObjects:[NSNumber numberWithFloat:-0.05],
				   [NSNumber numberWithFloat:0.05],
				   nil];

	int randomNum = arc4random() % 10;
	
    anim.duration = 0.09f + (randomNum * 0.01f);
    anim.autoreverses = YES;
    anim.repeatCount = HUGE_VALF;
    [self.layer addAnimation:anim forKey:@"wiggleRotation"];
    
    anim = [CAKeyframeAnimation animationWithKeyPath:@"transform.translation.y"];
    anim.values = [NSArray arrayWithObjects:[NSNumber numberWithFloat:-1],
                   [NSNumber numberWithFloat:1],
                   nil];
    anim.duration = 0.07f + (randomNum * 0.01f);
    anim.autoreverses = YES;
    anim.repeatCount = HUGE_VALF;
    anim.additive = YES;
    [self.layer addAnimation:anim forKey:@"wiggleTranslationY"];
}
- (void)stopWiggling {
    [self.layer removeAnimationForKey:@"wiggleRotation"];
    [self.layer removeAnimationForKey:@"wiggleTranslationY"];
}

- (BOOL)beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
	
	return YES;
}

- (void)endTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
}

@end
