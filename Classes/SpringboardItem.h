//
//  SpringboardItem.h
//  demo
//
//  Created by Soon Leung on 01/02/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface SpringboardItem : UIControl {	
	id _delegate;
	
	UIButton *_icon;
	NSString *_title;
}

@property(nonatomic,assign) id delegate;
@property(nonatomic,retain) NSString *title;

- (id)initWithTitle:(NSString *)title image:(UIImage *)image;

- (void)appearDraggable;
- (void)appearNormal;
- (void)startWiggling;
- (void)stopWiggling;

@end
