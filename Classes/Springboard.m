//
//  SpringboardViewController.m
//  demo
//
//  Created by Soon Leung on 01/02/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

@interface CustomScrollView : UIScrollView
{
	
}

@end

@implementation CustomScrollView 

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event { 
	UITouch *touch = touches.anyObject;
	
	CGPoint lastTouchPosition = [touch previousLocationInView:self]; 
	CGPoint currentTouchPosition = [touch locationInView:self]; 
	
	int diffX = currentTouchPosition.x - lastTouchPosition.x;
	int currentX = self.contentOffset.x;
	
	[self setContentOffset:CGPointMake(currentX-diffX, 0)];
}
	
@end

#import "Springboard.h"

#import "demoAppDelegate.h"

#import "SpringboardItem.h"
#import <QuartzCore/QuartzCore.h>

static int kButtonPerRow = 3;
static int kButtonPerColumn = 3;

static int kMarginToChangePage = 20;

@implementation Springboard

@synthesize delegate		= _delegate;
@synthesize items			= _items; 

@synthesize isCustomizing	= _isCustomizing;

#pragma mark -
#pragma mark Customization
- (void)beginCustomizing:(UITouch *)touch {
	if ([_delegate respondsToSelector:@selector(springboard:willBeginCustomizingItems:)]) {
		[_delegate springboard:self willBeginCustomizingItems:_itemForFrameRearrangeContainer];
	}
	
	_heldItem = ((SpringboardItem *)touch.view);
	[_heldItem appearDraggable];

	heldFrameIndex = 0;
	for (int i = 0; i < [_items count]; ++i) {
        if (((SpringboardItem *)[_itemForFrameRearrangeContainer objectAtIndex:i]).tag == _heldItem.tag) {
            heldFrameIndex =  i;
        }
    }
	
	// Move its layer to front
	CALayer *superlayer = _heldItem.layer.superlayer;
    [_heldItem.layer removeFromSuperlayer];
    [superlayer addSublayer:_heldItem.layer];
	
	// animate remaining items
	if (!_isCustomizing) {
		_isCustomizing = YES;		
		for (SpringboardItem *anItem in _items) {
			if (anItem != _heldItem) {
				[anItem startWiggling];
			}
		}
	}
}
- (void)endCustomizing {
	if (!_isCustomizing) {
		return;
	}
	
	_isCustomizing = NO;
	for (SpringboardItem *anItem in _items) {
		if (anItem != _heldItem) {
			[anItem stopWiggling];
		}
	}
	
	if ([_delegate respondsToSelector:@selector(springboard:didEndCustomizingItems:)]) {
		[_delegate springboard:self didEndCustomizingItems:_itemForFrameRearrangeContainer];
	}
}

#pragma mark -
#pragma mark UIResponder methods override
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event { 
	UITouch *touch = [touches anyObject]; 
	_heldItem = ((SpringboardItem *)touch.view);
	[_heldItem appearDraggable];
	[_heldItem stopWiggling];
	
	heldFrameIndex = 0;
	for (int i = 0; i < [_items count]; ++i) {
        if (((SpringboardItem *)[_itemForFrameRearrangeContainer objectAtIndex:i]).tag == _heldItem.tag) {
            heldFrameIndex = i;
        }
    }

	// Move its layer to front
	CALayer *superlayer = _heldItem.layer.superlayer;
    [_heldItem.layer removeFromSuperlayer];
    [superlayer addSublayer:_heldItem.layer];
}
-(void)animationDidStop {
	adjustingPage = NO;
}
-(void)adjustPaging:(BOOL)toRight {
	if (adjustingPage) {
		return;
	}
	
	adjustingPage = YES;	
	int offsetX = _customizeView.contentOffset.x;
	int pageWidth = CGRectGetWidth(_customizeView.frame);
	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.5];
	[UIView setAnimationTransition:UIViewAnimationTransitionNone
						   forView:_customizeView
							 cache:YES];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(animationDidStop)];
	
	[_customizeView setContentOffset:CGPointMake((toRight?offsetX + pageWidth: offsetX - pageWidth) ,0)];
	
	[UIView commitAnimations];
}
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event { 
	UITouch *touch = [touches anyObject]; 
	_heldItem = ((SpringboardItem *)touch.view);
	
	if (!_isCustomizing) {
		[_customizeView touchesMoved:touches withEvent:(UIEvent *)event];
		return;
	}

	[CATransaction begin];
    [CATransaction setDisableActions:TRUE];
    _heldItem.center = [touch locationInView:_customizeView];
    [CATransaction commit];
	
	// Adjust current page
	CGFloat pageWidth = CGRectGetWidth(_customizeView.frame);
	float fractionalPage = _heldItem.center.x / pageWidth;
	NSInteger lowerNumber = floor(fractionalPage);
	NSInteger upperNumber = lowerNumber + 1;
	double btnPerPage = kButtonPerRow*kButtonPerColumn;
	int numOfPage =  ceil((double)[_items count]/btnPerPage);
	
//	NSLog(@"lowerNumber:%d upperNumber:%d", lowerNumber, upperNumber);
//	NSLog(@"touch locationInView:window:%f", [touch locationInView:((demoAppDelegate *)[[UIApplication sharedApplication] delegate]).window].x);

	if ([touch locationInView:_customizeView].x > [touch previousLocationInView:_customizeView].x) {
		if ([touch locationInView:((demoAppDelegate *)[[UIApplication sharedApplication] delegate]).window].x > 320-kMarginToChangePage) {
			if (upperNumber < numOfPage) {
				// Move to right
				[self adjustPaging:YES];
			}
		}
	}
	else if ([touch locationInView:_customizeView].x < [touch previousLocationInView:_customizeView].x) {
		if ([touch locationInView:((demoAppDelegate *)[[UIApplication sharedApplication] delegate]).window].x < kMarginToChangePage) {
			if (lowerNumber > 0) {
				// Move to left
				[self adjustPaging:NO];
			}
		}
	}
	
	if (adjustingPage) {
		return;
	}
		
	// Rearrange buttons	
	int frameIndex = 0;
	float minDist = FLT_MAX;
    for (int i=0; i<[_items count]; ++i) {
        CGRect frame = [[_itemFrameContainer objectAtIndex:i] CGRectValue];
        
        float dx = [touch locationInView:_customizeView].x - CGRectGetMidX(frame);
        float dy = [touch locationInView:_customizeView].y - CGRectGetMidY(frame);
        
        float dist = (dx * dx) + (dy * dy);
        if (dist < minDist) {
            frameIndex = i;
            minDist = dist;
        }
    }	
	
    if (frameIndex != heldFrameIndex) {
		[UIView beginAnimations:nil context:nil];
		
        if (frameIndex < heldFrameIndex) {
            for (int i = heldFrameIndex; i > frameIndex; --i) {
                SpringboardItem *movingItem = [_itemForFrameRearrangeContainer objectAtIndex:i-1];
                movingItem.frame = [[_itemFrameContainer objectAtIndex:i] CGRectValue];
				
				[_itemForFrameRearrangeContainer replaceObjectAtIndex:i withObject:movingItem];
            }
        }
        else if (heldFrameIndex < frameIndex) {
            for (int i=heldFrameIndex; i<frameIndex; ++i) {
                SpringboardItem *movingItem = [_itemForFrameRearrangeContainer objectAtIndex:i+1];
                movingItem.frame = [[_itemFrameContainer objectAtIndex:i] CGRectValue];

				[_itemForFrameRearrangeContainer replaceObjectAtIndex:i withObject:movingItem];
            }
        }
        heldFrameIndex = frameIndex;        
		[_itemForFrameRearrangeContainer replaceObjectAtIndex:heldFrameIndex withObject:_heldItem];

		[UIView commitAnimations];
    }
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {	
	if (_heldItem) {
        [_heldItem appearNormal];
		[_heldItem startWiggling];
        _heldItem.frame = [[_itemFrameContainer objectAtIndex:heldFrameIndex] CGRectValue];
        _heldItem = nil;
    }
}

#pragma mark -
#pragma mark Initialization
-(id) initObjectFromPlistFile:(NSString *)filePath {
	NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
															  NSUserDomainMask, YES) objectAtIndex:0];
	
	NSString *plistPath = [rootPath stringByAppendingPathComponent:filePath];
	if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath]) {
		plistPath = [[NSBundle mainBundle] pathForResource:filePath ofType:nil];
	}
	
	NSData *plistData = [[NSFileManager defaultManager] contentsAtPath:plistPath];
	
	NSString *errorDesc = nil;
	NSPropertyListFormat format;		
	id result = [[NSPropertyListSerialization
				  propertyListFromData:plistData
				  mutabilityOption:NSPropertyListMutableContainersAndLeaves
				  format:&format
				  errorDescription:&errorDesc] retain];
	
	if (!result) {
		NSLog(@"Error reading plist: %@, format: %d", errorDesc, format);
	}
	return result;
}
- (void)initIcons {
	NSMutableArray *tmpArray = [[NSMutableArray alloc] init];
	
	NSDictionary *iconKeysDict = [self initObjectFromPlistFile:@"IconList.plist"];	
	NSLog(@"iconKeysDict:%@", iconKeysDict);
	
	NSArray *iconSequence = [[NSUserDefaults standardUserDefaults] arrayForKey:@"iconSequence"];
	NSEnumerator *enumerator = [iconSequence objectEnumerator];	
	id object;
	while (object = [enumerator nextObject]) {
		UIImage *aBtnImg = [UIImage imageNamed:[iconKeysDict objectForKey:object]];
		SpringboardItem *anItem = [[SpringboardItem alloc] initWithTitle:object image:aBtnImg];
		[tmpArray addObject:anItem];
		[anItem release];
	}	
	[_items release];
	_items = [tmpArray retain];
	[tmpArray release];
	
	double btnPerPage = kButtonPerRow*kButtonPerColumn;
	int numOfPage =  ceil((double)[_items count]/btnPerPage);
	NSLog(@"numOfPage: %d", numOfPage);
	
	int widthBetweenCenterOfButtons = ITEM_FRAME_SIZE.width;
	int heightBetweenCenterOfButtons = ITEM_FRAME_SIZE.height;
	int startPtX = (CGRectGetWidth(self.frame)-ITEM_FRAME_SIZE.width*kButtonPerRow)/2;
	int startPtY = (CGRectGetHeight(self.frame)-ITEM_FRAME_SIZE.height*kButtonPerColumn)/2;

	CGPoint centerOfButton;
	
	int btnIndex = 0;
	for (SpringboardItem *anItem in _items) {
		int currentPage = btnIndex/btnPerPage+1;
		NSLog(@"btnCount: %d", btnIndex);
		NSLog(@"currentPage: %d", currentPage);
		
		// Caculate for the center point
		if (btnIndex == 0) {
			centerOfButton = CGPointZero;
			centerOfButton.x = startPtX + (int)widthBetweenCenterOfButtons/2;
			centerOfButton.y = startPtY + (int)heightBetweenCenterOfButtons/2;
		}
		else {
			if (btnIndex%kButtonPerRow == 0) {
				centerOfButton.x = startPtX + (currentPage-1)*CGRectGetWidth(_customizeView.frame)+(int)widthBetweenCenterOfButtons/2;
				centerOfButton.y += heightBetweenCenterOfButtons;
				
				if (btnIndex%(kButtonPerRow*kButtonPerColumn) == 0) {
					centerOfButton.y = startPtY + (int)heightBetweenCenterOfButtons/2;
				}
			}
			else {
				centerOfButton.x += widthBetweenCenterOfButtons;
			}
		}
		NSLog(@"centerOfButton: %@", NSStringFromCGPoint(centerOfButton));
		
		anItem.center = centerOfButton;
		anItem.delegate = self;
		[_customizeView.layer addSublayer:anItem.layer];		
		[_itemFrameContainer addObject:[NSValue valueWithCGRect:anItem.frame]];
		[_itemForFrameRearrangeContainer addObject:anItem];
		anItem.tag = btnIndex++;
	}
	
	[_customizeView setContentSize:CGSizeMake(numOfPage*CGRectGetWidth(_customizeView.frame), CGRectGetHeight(_customizeView.frame))];
}
- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        // Initialization code
		_customizeView = [[CustomScrollView alloc] initWithFrame:frame];
		[self addSubview:_customizeView];
		_customizeView.backgroundColor = [UIColor lightGrayColor];
		_customizeView.pagingEnabled = YES;
		_customizeView.showsVerticalScrollIndicator = NO;
		_customizeView.showsHorizontalScrollIndicator = NO;
		
		UIButton *bgBtn = [UIButton buttonWithType:UIButtonTypeCustom];
		bgBtn.frame = frame;
		[_customizeView addSubview:bgBtn];
		[bgBtn addTarget:self action:@selector(endCustomizing) forControlEvents:UIControlEventTouchUpInside];
				
		_delegate = nil;
		_items = nil;	
		_isCustomizing = NO;

		_itemFrameContainer = [[NSMutableArray alloc] init];
		_itemForFrameRearrangeContainer = [[NSMutableArray alloc] init];
		
		adjustingPage = NO;
		
		[self initIcons];
		
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(endCustomizing) name:UIApplicationDidEnterBackgroundNotification object:nil];
    }
    return self;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	
	[_customizeView release];
	_delegate = nil;
	[_items release];
	
	[_itemFrameContainer removeAllObjects];
	[_itemFrameContainer release];
	[_itemForFrameRearrangeContainer removeAllObjects];
	[_itemForFrameRearrangeContainer release];
	
    [super dealloc];
}

#pragma mark -
#pragma mark Button action
-(void)buttonAction:(id)sender {
	if (!_isCustomizing && [_delegate respondsToSelector:@selector(springboard:didSelectItem:)]) {
		[_delegate springboard:self didSelectItem:sender];
	}
}
-(void)adjustToCurrentPage {
	int currentOffsetX = _customizeView.contentOffset.x;
	int pageWidth = CGRectGetWidth(_customizeView.frame);	
	int currentPage = currentOffsetX/pageWidth;
	
	if (currentOffsetX%pageWidth > CGRectGetWidth(_customizeView.frame)/2) {
		[_customizeView setContentOffset:CGPointMake((currentPage+1)*pageWidth, 0) animated:YES];
	}
	else {
		[_customizeView setContentOffset:CGPointMake(currentPage*pageWidth, 0) animated:YES];
	}
}

@end
