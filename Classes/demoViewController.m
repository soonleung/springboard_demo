    //
//  demoViewController.m
//  demo
//
//  Created by Soon Leung on 01/02/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "demoViewController.h"

#import "Springboard.h"
#import "SpringboardItem.h"

@implementation demoViewController

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/


// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
	UIView *tmpView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
	self.view = tmpView;
	[tmpView release];
	
	Springboard *vc = [[Springboard alloc] initWithFrame:self.view.frame];
	vc.delegate = self;
	[self.view addSubview:vc];
	[vc release];
}


/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


#pragma mark -
#pragma mark SpringboardDelegate
- (void)springboard:(Springboard *)sp didSelectItem:(SpringboardItem *)item {
	NSLog(@"didSelectItem:%@", item.title);
}

- (void)springboard:(Springboard *)sp willBeginCustomizingItems:(NSArray *)items {
	NSLog(@"willBeginCustomizingButtons");
	for (SpringboardItem *anItem in items) {
		NSLog(@"%@", anItem.title);
	}
}
- (void)springboard:(Springboard *)sp didEndCustomizingItems:(NSArray *)items {
	NSLog(@"didEndCustomizingButtons");
	
	NSMutableArray *currentIconList = [[NSMutableArray alloc] init];
	for (SpringboardItem *anItem in items) {
		NSLog(@"%@", anItem.title);
		[currentIconList addObject:anItem.title];
	}
	
	[[NSUserDefaults standardUserDefaults] setObject:currentIconList forKey:@"iconSequence"];
	[[NSUserDefaults standardUserDefaults] synchronize];
}

@end
