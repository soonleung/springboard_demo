//
//  demoAppDelegate.h
//  demo
//
//  Created by Soon Leung on 01/02/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class demoViewController;

@interface demoAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
	demoViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;

@end

