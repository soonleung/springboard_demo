//
//  GTWebImageData.h
//  HongKongMovie
//
//  Created by Luke Chan on 06/07/2009.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

enum GTWebImageDataState {
	GTWebImageDataStateNoData,
	GTWebImageDataStateDownloading,
	GTWebImageDataStateReady,
};

@interface GTWebImageData : NSObject <NSCoding>{
	UIImage* image;
	NSData* imageData;
	NSString* imageString;
	NSString *imageLocalPath;
	NSMutableArray* receiverArray;
	BOOL isCache;
	enum GTWebImageDataState dataState; 
}

@property (nonatomic, retain) UIImage* image;
@property (nonatomic, retain) NSData* imageData;
@property (nonatomic, retain) NSString* imageString;
@property (nonatomic, retain) NSMutableArray* receiverArray;
@property (nonatomic, retain) NSString *imageLocalPath;
@property (nonatomic, assign) BOOL isCache;
@property enum GTWebImageDataState dataState; 


@end
