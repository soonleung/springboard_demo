//
//  GTImageBank.h
//  HongKongMovie
//
//  Created by Luke Chan on 06/07/2009.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GTWebImageData.h"

#define DEFAULT_DIRECTORY @"ImageBank"

@interface GTImageBank : NSObject {
	NSMutableDictionary* imageBank;
	NSFileManager *fileManager;
	NSString *directoryPath;
}

+(id) imageBank; // singleton implementation

-(void) deReceiver:(id) receiver;
-(UIImage*) getImageFromString:(NSString*) imageString;

-(void) newThreadForceDownloadImage:(GTWebImageData*) imageData;
-(void) newThreadDownloadImage:(GTWebImageData*) imageData;

-(void) forceDownloadImage:(NSString*) imageString andAddReceiver:(id) receiver isCache:(BOOL)isCache;
-(void) downloadImage:(NSString*) imageString andAddReceiver:(id) receiver isCache:(BOOL)isCache;

//-(void) restoreImageBankFromFile;
//-(void) storeImageBankToFile;

-(void) storeImageData:(GTWebImageData*)imageData toFile:(NSString*)filePath;
-(BOOL) restoreImageDataWithPath:(NSString*)path;
-(void) clearSavedImageBank;
- (void)clearCached;
@end
