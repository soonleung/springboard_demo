//
//  GTHTTPRequest.h
//  GTHTTPConnectionSample
//
//  Created by Ken on 30/11/2010.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GTNetworkRequest.h"

@interface GTHTTPRequest : GTNetworkRequest {
	NSMutableURLRequest *urlRequest;
	NSString			*urlPath;
	//CFStringEncodings encoding;
}

@property (readonly) NSMutableURLRequest *urlRequest;
@property (nonatomic, retain) NSString			  *urlPath;

-(id) initWithURLString:(NSString*)inputURL;
//-(void) setRequestEncoding:(CFStringEncodings)inputEncoding;

@end
