//
//  GTURLConnection.h
//  GTHTTPConnectionSample
//
//  Created by Ken Wong on 26/11/2010.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GTXMLParser.h"
#import "GTXMLUnitData.h"
#import "GTNetworkProtocol.h"

@class GTHTTPRequest;

@interface GTURLConnection : NSObject {
	NSURLConnection *_connection;
	BOOL _isDone;
	BOOL _isBlocking;
	
	NSMutableData *downloadData;
	
	GTXMLParser *parser;
		
	id<GTURLConnectionDelegate>	delegate;
}

@property (readonly) NSMutableData *downloadData; 
@property (readonly) GTXMLParser *parser;
@property (nonatomic, assign) id<GTURLConnectionDelegate>	delegate;

-(void) prepareParser;
-(NSData*) sendRequest:(GTHTTPRequest *)urlRequest blocking:(BOOL)blockingFlag;


@end
