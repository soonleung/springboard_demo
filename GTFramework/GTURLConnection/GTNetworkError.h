//
//  GTNetworkError.h
//  GTHTTPConnectionSample
//
//  Created by Ken Wong on 30/11/2010.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface GTNetworkError : NSObject {
	NSURLResponse *response;
	NSError *error;
	BOOL isError;
}

@property (nonatomic, retain) NSURLResponse *response;
@property (nonatomic, retain) NSError *error;
@property BOOL				  isError;
@end
