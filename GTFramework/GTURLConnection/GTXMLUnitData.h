//
//  UnitData.h
//  GTXMLParser
//
//  Created by Ken Wong on 22/11/2010.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface GTXMLUnitData : NSObject {
	NSString		*key;
	NSMutableString	*value;
	GTXMLUnitData		*parent;
	NSMutableDictionary	*attributeList;
	NSMutableArray  *childList;
}

@property (nonatomic, retain) NSString				*key;
@property (nonatomic, retain) NSMutableString		*value;
@property (nonatomic, assign) GTXMLUnitData			*parent;
@property (nonatomic, retain) NSMutableDictionary	*attributeList;
@property (nonatomic, assign) NSMutableArray		*childList;

@end
