//
//  GTNetworkRequest.h
//  GTHTTPConnectionSample
//
//  Created by Ken Wong on 29/11/2010.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface GTNetworkRequest : NSObject {
#define GT_DEFAULT_REQUEST_TIMEOUT	20
	double timeout;
	NSStringEncoding encoding;
}

@property double timeout;
@property NSStringEncoding encoding;

-(void) commitRequest;
+(NSString *) URLEncodeString:(NSString *)string encoding:(CFStringEncoding)encodingType;
-(void) setRequestEncoding:(CFStringEncoding)inputEncoding;

@end
