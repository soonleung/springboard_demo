//
//  GTHTTPGetRequest.h
//  GTHTTPConnectionSample
//
//  Created by Ken on 30/11/2010.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GTHTTPRequest.h"


@interface GTHTTPGetRequest : GTHTTPRequest {
	NSMutableString *parameterString;
}

@property (readonly) NSMutableString *parameterString;

-(void) addParameterKey:(NSString*)key value:(NSString*)value;


@end
