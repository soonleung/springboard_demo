//
//  GTNetworkProtocol.h
//  GTHTTPConnectionSample
//
//  Created by Ken Wong on 30/11/2010.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "GTNetworkError.h"

@protocol GTURLConnectionDelegate
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response error:(GTNetworkError*)error;
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data;
- (void)connection:(NSURLConnection *)connection didFailWithError:(GTNetworkError *)error;
- (void)connectionDidFinishLoading:(NSURLConnection *)connection;
@end

