//
//  GTHTTPPostRequest.h
//  GTHTTPConnectionSample
//
//  Created by Ken Wong on 29/11/2010.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GTHTTPRequest.h"


//typedef enum {
//	REQUEST_GET	= 0,
//	REQUEST_POST	
//} GTHTTPPostRequestType;

@interface GTHTTPPostRequest : GTHTTPRequest {
#define HTTP_MULTIPART_BOUNDARY		@"17283718471898741874"
	NSMutableData *postBody;
}

-(void) addParameterKey:(NSString*)key value:(NSString*)value;
-(void) addImage:(NSData*)imageData tag:(NSString*)tagName filename:(NSString*)filename;
-(void) addVideo:(NSData *)videoData tag:(NSString*)tagName filename:(NSString *)filename;

 
@end
