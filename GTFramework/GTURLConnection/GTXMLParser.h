//
//  GTXMLParser.h
//  GTXMLParser
//
//  Created by Ken Wong on 22/11/2010.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <libxml/tree.h>
#import "GTNetworkProtocol.h"

@class GTXMLUnitData;
@class GTHTTPRequest;

@protocol GTXMLParserDelegate
-(void) XMLParserDidFinishParsing:(id)parser;
-(void) XMLParserDidFailParsing:(id)parser error:(GTNetworkError*)error;
@end

@interface GTXMLParser : NSObject {
	
#define TAG_NUM_COUNT	200
	
	BOOL _isDone;
	BOOL _isBlocking;
	NSURLConnection *_connection;
    xmlParserCtxtPtr _context;
	
	GTNetworkError *networkError;

	GTXMLUnitData *treeHeadObj;
	GTXMLUnitData *_treeCurrentObj;
		
	id<GTXMLParserDelegate>	parserDelegate;
	id<GTURLConnectionDelegate> connectionDelegate;
	
	BOOL _isFileParseDone;
}

@property (nonatomic, assign) id<GTXMLParserDelegate>	parserDelegate;
@property (nonatomic, assign) id<GTURLConnectionDelegate> connectionDelegate;
@property (readonly)		  GTXMLUnitData *treeHeadObj;

//Internal functions:
//-(void) _sendRequest:(NSArray*)paraList;
-(void) _sendRequest:(GTHTTPRequest*)request;
-(void) _recursiveTraverse:(GTXMLUnitData*)parentData tagName:(NSString*)tagName value:(NSString*)value resultList:(NSMutableArray*)resultList;
-(void) _recursiveTraverse:(GTXMLUnitData*)parentData tagName:(NSString*)tagName value:(NSString*)value attributes:(NSDictionary*)attrDict resultList:(NSMutableArray*)resultList;
-(void) _unloadSearchTree;
-(GTXMLUnitData*) _recursiveUnload:(GTXMLUnitData*)currentPtr;


//Functions to be used:
-(GTNetworkError*) parseXMLFile:(NSString*)filePath;
-(GTNetworkError*) parseXMLFileData:(NSData*)fileData;
//-(GTXMLUnitData*) downloadAndParse:(NSString *)urlString blocking:(BOOL)isBlocking;
//-(GTXMLUnitData*) downloadAndParse:(NSString *)urlString methodType:(NSString*)methodType timeout:(double)timeout blocking:(BOOL)blockingFlag;
-(GTNetworkError*) downloadAndParse:(GTHTTPRequest*)request blocking:(BOOL)blockingFlag;
-(NSMutableArray*) fullSearchForDataByTag:(NSString*)tag value:(NSString*)value;
-(NSMutableArray*) fullSearchForDataByTag:(NSString*)tag value:(NSString*)value attributes:(NSDictionary*)attrDict;
-(NSMutableArray*) partialSearchWithData:(GTXMLUnitData*)untiData ByTag:(NSString*)tag value:(NSString*)value;
-(NSMutableArray*) partialSearchWithData:(GTXMLUnitData*)untiData ByTag:(NSString*)tag value:(NSString*)value attributes:(NSDictionary*)attrDict;

@end






